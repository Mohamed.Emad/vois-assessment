import pyspark
import sys
from pyspark.sql.types import *

# A Spark Session is how we interact with Spark SQL to create Dataframes
from pyspark.sql import SparkSession

# This will help catch some PySpark errors
from py4j.protocol import Py4JJavaError

#Read CSV file into datafram
def readCSV(inputUri,sep,header,spark):
    df = spark.read.csv(inputUri,sep=sep, header=header)
    return df

# Create a SparkSession under the name "voisAssment". 
spark = SparkSession.builder.appName("voisAssment").getOrCreate()

# Create a six column schema 
fields = [StructField("device_tac", IntegerType(), True),
          StructField("date_time_stamp", TimestampType(), True),
          StructField("energyconsumption", IntegerType(), True),
          StructField("trafficlevel", LongType(), True),
           StructField("week", IntegerType(), True),
          StructField("performance", FloatType(), True)
          ]
schema = StructType(fields)


# Create an empty DataFrame. We will continuously union our output with this
radio_wizard_table = spark.createDataFrame([], schema)

spark.conf.set("viewsEnabled","true")
spark.conf.set("materializationDataset","vois")

# the big query query
query = """ SELECT device_tac,date_time_stamp,
energyconsumption,trafficlevel,EXTRACT(WEEK FROM date_time_stamp) AS week,
 trafficlevel/energyconsumption AS performance FROM 
 voisassment.vois.Radio_Wizard """

radio_wizard_df = (spark.read.format('bigquery').option("query", query)
                        .load())



radio_wizard_table = (radio_wizard_df)

GSMA_df=readCSV(inputUri="gs://vois-bucket/GSMA.CSV",sep=";",header=True,spark=spark)


full_df=GSMA_df.join(radio_wizard_table,"device_tac","inner") 
    

full_df=full_df.sort("performance")
full_df.head(10)
outputPath="gs://vois-bucket/worst_performing_devices.CSV"
full_df=full_df.select('week','device_tac', 'device_manufacturer',
'energyconsumption', 'trafficlevel', 'performance')
full_df.write.csv(outputPath)

