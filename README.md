# Vois Assessment

This repository created for sharing the VOIS assessment code with VOIS team

# Description of the code
This code is to idetify ten worst performing device models per week.
This code excute in data proc

# Code Excution 
To excute this script you have to run the below command 
gcloud dataproc jobs submit pyspark --cluster {clusterName}  --region {region}  --jars=gs://spark-lib/bigquery/spark-bigquery-latest_2.12.jar --driver-log-levels root=FATAL   {file name}

# Pre Requests 
- Bigquery table with the schema mentioned in the Assmessment description file 
- The schema of GSMA mentioned in the Assmessment description file and you have to store in GCS and you have to add file path in the script
